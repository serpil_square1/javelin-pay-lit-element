import { html, css, LitElement } from 'lit-element';

class JavelinPay extends LitElement {
  static get styles() {
    return css`
      *, :after, :before {
        box-sizing: border-box;
        border: 0 solid #e4e4e7;
      }
      .widget {
        line-height: inherit;
        font-family: -apple-system, BlinkMacSystemFont, Nunito, "Segoe UI", Roboto,
          "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
          "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        box-sizing: border-box;
        border-width: 0;
        border-style: solid;
        background-color: #fff;
        border-color: transparent;
        border-radius: .75rem;
        height: 32rem;
        margin-left: auto;
        margin-right: auto;
        margin-top: 2rem;
        position: relative;
        box-shadow: 0 0 #0000, 0 0 #0000, 0 25px 50px -12px rgba(0, 0, 0, 0.25);
        width: 20rem;
      }
      .product-image {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        width: 100%;
        overflow: hidden;
        height: 13rem;
        border-color: transparent;
        border-top-left-radius: .75rem;
        border-top-right-radius: .75rem;
      }
      .product-body {
        margin-top: 2rem;
        text-align: center;
      }
      h1 {
        letter-spacing: .025em;
        color: rgb(32,38,48);
        line-height: 1.75rem;
        font-size: 1.125rem;
        font-weight: 600;
        display: block;
      }
      h2 {
        letter-spacing: .025em;
        color: rgb(32,38,48);
        line-height: 1.75rem;
        font-size: 1.125rem;
        font-weight: 400;
        display: block;
      }
      h4 {
        margin-top: 1.25rem;
        letter-spacing: .05em;
        color: rgba(32,38,48);
        font-size: 1.875rem;
        line-height: 2.25rem;
        font-weight: 700;
      }
      .product-footer {
        position: absolute;
        left: 0;
        bottom: 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        width: 100%;
        background-color: rgb(245,251,255);
        height: 7rem;
        border-bottom-left-radius: .75rem;
        border-bottom-right-radius: .75rem;
      }
      .logo > p {
        color: rgb(32,38,48);
        font-size: .625rem;
      }
      .logo > svg {
        width: 3rem;
      }
      button {
        max-width: 60%;
        overflow: hidden;
        text-overflow: ellipsis;
        color: #fff;
        padding-left: 2rem;
        padding-right: 2rem;
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
        font-size: 1rem;
        line-height: 1.5rem;
        font-weight: 600;
        float: right;
        border-radius: 9999px;
        outline: none;
      }
      button:hover {
        opacity: 0.9;
        cursor: pointer;
      }
    `;
  }

  static get properties() {
    return {
      data: {type: Object},
      loading: {type: Boolean},
      product: {attribute: true, type: String},
      "button-text": {attribute: true, type: String},
      "button-color": {attribute: true, type: String}
    };
  }

  constructor() {
    super();
    this.data = {};
    this.loading = true;
    this.product = null;
    this.["button-text"] = "Buy Now";
    this.["button-color"] = "rgb(59,137,247)";
  }

  async connectedCallback() {
    super.connectedCallback();
    if (this.product) {
      await this.getProduct();
    }
  }

  async getProduct() {
    const response = await fetch(`https://staging.javelinhq.com/api/product/${this.product}`, {
      method: 'GET',
      headers: {
        Accept: `application/vnd.xyzpay.v1+json`,
      },
      credentials: 'include'
    });
    const { data } = await response.json();

    if (data) {
      this.data = data;
      this.loading = false;
    }
  }


  _handleClick() {
    window.open(`https://admin.staging.javelinhq.com/product/${this.product}`);
  }

  javelinLogo() {
    return html`<svg xmlns="http://www.w3.org/2000/svg" width="50" height="16" viewBox="0 0 50 16"><g fill="none" fill-rule="evenodd"><g fill="#596077" fill-rule="nonzero"><g><g><g><path d="M.167 15.904c1.675 0 2.904-.357 3.688-1.071.784-.715 1.176-1.836 1.176-3.365V.335H2.469v11.292c0 1.4-.823 2.1-2.469 2.1l.167 2.177zm9.217-2.846c.536 0 1.028-.095 1.478-.285.449-.19.83-.449 1.142-.778l.16.896h2.025V8.052c0-.747-.165-1.399-.494-1.954-.33-.555-.791-.985-1.385-1.29-.595-.303-1.296-.455-2.106-.455-.507 0-1.043.067-1.607.2-.563.135-1.135.341-1.716.62l.829 2.034c.68-.508 1.359-.762 2.034-.762 1.138 0 1.8.455 1.984 1.365-.257-.045-.512-.081-.766-.11-.254-.027-.495-.041-.724-.041-.726 0-1.37.128-1.934.385-.563.257-1.006.606-1.326 1.046-.321.441-.482.94-.482 1.499 0 .73.27 1.325.808 1.783.539.457 1.232.686 2.08.686zm.695-2.093c-.352 0-.635-.085-.85-.255-.215-.17-.322-.395-.322-.674 0-.307.133-.544.398-.711.265-.168.634-.251 1.109-.251.229 0 .462.02.699.058.237.04.459.092.665.16v.167c0 .424-.163.781-.49 1.071-.326.29-.73.435-1.21.435zM20.4 12.891l3.306-8.37H21.17l-1.398 4.076c-.078.223-.169.535-.272.937-.104.402-.189.767-.256 1.097h-.033c-.056-.33-.133-.698-.23-1.105-.098-.408-.186-.717-.264-.93L17.203 4.52h-2.62l3.474 8.37H20.4zm7.743.167c.58 0 1.187-.095 1.82-.285.634-.19 1.274-.538 1.922-1.046l-1.457-1.741c-.312.296-.66.537-1.042.724-.382.187-.863.28-1.444.28-.541 0-1.014-.145-1.419-.435-.404-.29-.682-.667-.832-1.13h6.529v-.728c0-.843-.19-1.59-.57-2.243-.38-.653-.897-1.167-1.552-1.54-.656-.374-1.405-.561-2.248-.561-.848 0-1.606.188-2.272.565-.667.376-1.193.89-1.578 1.54s-.578 1.39-.578 2.222c0 .843.201 1.595.603 2.256.402.661.957 1.18 1.666 1.557.708.377 1.526.565 2.452.565zm1.599-4.972H25.64c.117-.48.364-.873.741-1.18.377-.307.822-.46 1.335-.46.508 0 .943.15 1.306.451.363.302.603.698.72 1.189zm6.036 4.805V0h-2.41v12.89h2.41zm2.805-9.878c.401 0 .725-.122.97-.368.246-.245.369-.57.369-.97 0-.403-.123-.726-.368-.972-.246-.245-.57-.368-.971-.368-.402 0-.726.123-.971.368-.246.246-.369.57-.369.971 0 .402.123.726.369.971.245.246.569.368.97.368zm1.205 9.878V4.52h-2.41v8.37h2.41zm4.01 0V8.647c0-.698.164-1.24.49-1.624.326-.385.752-.578 1.277-.578 1.015 0 1.523.678 1.523 2.034v4.412h2.419V8.01c0-1.15-.29-2.047-.87-2.692-.58-.644-1.36-.966-2.336-.966-.552 0-1.059.103-1.52.31-.46.206-.854.499-1.184.878l-.1-1.02h-2.11v8.37H43.8z" transform="translate(-965 -805) translate(290 245) translate(645 100) translate(30 442) translate(0 18)"/></g></g></g></g></g></svg>`}


  render() {
    return html`
      ${this.loading?
        html`<div></div>` :
        html`
          <div class="widget">
            <div class="product-image" style="background-image: url(${this.data.image});"></div>
            <hr style="width: 100%; border-top: 0 solid #e4e4e7; margin: 1rem 0 2rem;" />
            <div class="product-body">
              <h1>${this.data.name}</h1>
              <h2>${this.data.description}</h2>
              <h4>€${Number.parseFloat((this.data.price || 0)/100).toFixed(2)}</h4>
              <span class="product-footer">
                <span class="logo">
                  <p>Powered by</p>
                  ${this.javelinLogo()}
                </span>
                <button type="button" @click="${this._handleClick}" style="background-color: ${this.["button-color"]};">
                  ${this.["button-text"]}
                </button>
              </span>
            </div>
          </div>
        `
      }
    `;
  }
}

window.customElements.define('javelin-pay', JavelinPay);
