import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import { terser } from 'rollup-plugin-terser';
import minifyHTML from 'rollup-plugin-minify-html-literals';
import { uglify } from 'rollup-plugin-uglify';
import filesize from 'rollup-plugin-filesize';

export default {
	input: 'index.js',
	output: {
		file: 'build/bundle.js',
		format: 'iife',
		sourcemap: true
	},
	plugins: [
		minifyHTML(),
		babel({
			babelHelpers: 'bundled'
		}),
		uglify(),
		resolve(),
		terser(),
		filesize({
			showGzippedSize: true,
			showBrotliSize: false,
			showMinifiedSize: true,
		})
	]
}
